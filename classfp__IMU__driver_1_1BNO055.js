var classfp__IMU__driver_1_1BNO055 =
[
    [ "__init__", "classfp__IMU__driver_1_1BNO055.html#a6eb176f186fd75b18ca73d27ee56ac1d", null ],
    [ "calib_status", "classfp__IMU__driver_1_1BNO055.html#a6311850ac5b7dca99579f3d9ff0abbdf", null ],
    [ "calibrate", "classfp__IMU__driver_1_1BNO055.html#ad8faebb4fdbc44a4845e6c5e7c1fc67c", null ],
    [ "change_mode", "classfp__IMU__driver_1_1BNO055.html#a9c57bc3f398199186b6967a97053b4fa", null ],
    [ "read_ang_vel", "classfp__IMU__driver_1_1BNO055.html#a5454e6647355957547497917cae7b0b3", null ],
    [ "read_coeff", "classfp__IMU__driver_1_1BNO055.html#a0a10aea205616a0db42087ce56054645", null ],
    [ "read_euler", "classfp__IMU__driver_1_1BNO055.html#ad807c30106446485dfda71b331916d5f", null ],
    [ "write_coeff", "classfp__IMU__driver_1_1BNO055.html#af9db75a82cdbba2a30aa6a4a4bf59906", null ]
];