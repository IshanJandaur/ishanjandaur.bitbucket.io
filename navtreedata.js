/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305: Mechatronics", "index.html", [
    [ "Labs", "index.html", [
      [ "Lab 2", "index.html#sec_1", null ],
      [ "Diagrams and State Machines", "index.html#Task", null ],
      [ "Lab 3", "index.html#sec_2", null ],
      [ "Lab 4", "index.html#sec_4", null ],
      [ "Lab 5", "index.html#sec_5", null ],
      [ "Video:", "index.html#sec_6", null ],
      [ "Final Term Project", "index.html#sec_7", null ]
    ] ],
    [ "Final Term Project Report", "sec_19.html", [
      [ "Introduction", "sec_19.html#sec_20", null ],
      [ "High Level Task Diagram", "sec_19.html#sec_21", null ],
      [ "Finite State Machines", "sec_19.html#sec_22", null ],
      [ "Procedure", "sec_19.html#sec_23", null ],
      [ "Potential Problems", "sec_19.html#sec_24", null ]
    ] ],
    [ "Homeworks 2 and 3", "sec_8.html", [
      [ "Homework 2", "sec_8.html#sec_9", null ],
      [ "Kinetic and Kinematic Diagrams", "sec_8.html#sec_10", null ],
      [ "MATLAB script to obtain final equations of motion", "sec_8.html#sec_11", null ],
      [ "Homework 3", "sec_8.html#sec_12", null ],
      [ "Open Loop Feedback Control", "sec_8.html#sec_13", null ],
      [ "Closed Loop Feedback Control", "sec_8.html#sec_14", null ]
    ] ],
    [ "Lab 4 Report: Using a Control Loop on Motor Duty Cycle", "sec_15.html", [
      [ "Introduction", "sec_15.html#sec_16", null ],
      [ "Block Diagram", "sec_15.html#sec_17", null ],
      [ "Plots Progression", "sec_15.html#sec_18", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Hwk2and3main_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';